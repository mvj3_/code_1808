public class MultiTouchActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		View v = new MultiTouchView(this);// 创建自己的view作为要显示的内容
		setContentView(v);
	}

	class MultiTouchView extends View {
		
		private float	x1;
		private float	x2;
		private float	y1;
		private float	y2;

		public MultiTouchView(Context context) {
			super(context);
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) {

			int counts = event.getPointerCount();// 取得触点个数
			Log.i("MultiTouchTest", "touches number = " + counts);
			
			x1 = event.getX();// 第一个触点的x坐标
			y1 = event.getY();// 第一个触点的y坐标
			x2 = event.getX(event.getPointerId(counts - 1));// 最后一个触点的x坐标
			y2 = event.getY(event.getPointerId(counts - 1));// 最后一个触点的y坐标

			invalidate();

			Log.i("MultiTouch", "x1= " + x1);
			Log.i("MultiTouch", "x2= " + x2);
			Log.i("MultiTouch", "y1= " + y1);
			Log.i("MultiTouch", "y2= " + y2);
			
			return true;
		}

		@Override
		protected void onDraw(Canvas canvas) {

			super.onDraw(canvas);
			Log.i("MultiTouch", "onDraw()");

			float r = (float) Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2)
					* (y1 - y2)) / 2;
			Log.i("MultiTouch", "r= " + r);
			r = 50 >= r ? 50 : r;
			Log.i("MultiTouch", "r= " + r);

			Paint paint = new Paint();
			paint.setColor(Color.BLUE);
			canvas.drawCircle((x1 + x2) / 2, (y1 + y2) / 2, r, paint);
		}
	}
}